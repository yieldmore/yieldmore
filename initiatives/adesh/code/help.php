<?php
extract(get_page_menu_variables());

section();
echo '	<h2>Areas where you can help others</h2>' . am_var('nl');
menu($menuIn, ['parent-slug' => $menuOf]);
section('end');

if ($menu1) {
	section();
	echo '	<h2>Help ' . humanize($menu1) . '</h2>' . am_var('nl');
	$sheet = get_sheet($menuAt . $menu1 .'.tsv', false);
	$cols = $sheet->columns;

	$helpTo = false;
	echo '<ul>' . am_var('nl');

	foreach ($sheet->rows as $item) {
		$name = urlize($item[$cols['name']]);
		echo '	<li>' . makeLink($item[$cols['name']], $menuOf . $menu1 . '/' . $name . '/', true) . '</li>' . am_var('nl');
		if ($name == $menu2) $helpTo = $item;
	}
	echo '</ul>' . am_var('2nl');
	section('end');
	
	if ($helpTo) {
		section();
		$columnNames = explode('	', 'name	referred-by	guardian	help-with	needs	per	upi	contact');
		foreach ($columnNames as $col) {
			$val = $helpTo[$cols[$col]];
			echo '<b>' . humanize($col) . '</b> &mdash; ' . renderSingleLineMarkdown($val, ['echo' => false]) . am_var('brnl');
		}
		section('end');
	}
}

