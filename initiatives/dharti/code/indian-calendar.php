<?php
$dataFolder = am_var('nodeFolder') . 'data/';

$sheet = get_sheet($dataFolder . '2025-jan.tsv', 'Cell');
$cols = $sheet->columns;

includeFeature('tables');
$cells = am_var('calendar-cells');

$output = getTableTemplate('month-calendar');

$cellTemplate = getTableTemplate('calendar-cell-3x3');
am_var('this-calendar-replaces', [
	'-empty-' => '<div class="grey"></div>',
]);

//Year	Month	Day	DOW	Row	Col	Cell	One	Two	Three	Four	Five	Six	Seven	Eight	Nine
$day9Cells = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];

foreach ($cells as $cell) {
	if (!isset($sheet->sections[$cell])) {
		$output = replaceCell($cell, '-empty-', $output);
		continue;
	}

	$cellContent = $cellTemplate;
	$cellData = $sheet->sections[$cell][0];
	foreach ($day9Cells as $inCell)
		$cellContent = str_replace('%Cell' . $inCell . '%', $cellData[$cols[$inCell]], $cellContent);

	$output = replaceCell($cell, $cellContent, $output);
}

function replaceCell($cell, $what, $html) {
	$what = replaceItems($what, am_var('this-calendar-replaces'));
	return str_replace('%cell-' . $cell . '%', $what, $html);
}

sectionId('indian-calendar');
echo $output;
section('end');
