## What is Project ARYA

Standing for "Awareness Resulting in Your Action", ARYA was in 2021, intended as a [youth led](%url%sophia/) social causes advocacy initiative where a team of students learn the skills needed to support a charity of their choice.

Today, we include [professionals / volunteers](%url%trainers/) AND students (after being trained) to help out.

<!--more-->

</section><section>

## For Charities

We offer volunteering per the [skills available](%url%skill-matrix/) in our resource pool / that we can find online, on whatsapp or by word of mouth.

Contact Imran on [whatsapp](https://wa.me/+919841223313/?text=arya+charity+volunteer+request) for an initial discussion.

Fill up <a href="javascript: void(0);" class="btn btn-primary btn-node toggle-engage engage-scroll" data-engage-target="engage-for-charities">this email engagement form </a> either ahead of or during the call with him.

</section><section>

## Empowering Change Makers (Pillars and Charities)

I'm Imran, [a technologist](https://www.linkedin.com/in/ianamazi/) trying to put my skills at the disposal of youngsters and society.

We want to work in collaboration with each other and build and online network together, sharing learnings, resources and professionals / volunteers / interns.

I have [devised a system](https://amadeusweb.com/) that quickly builds a website for businesses, charities and students, all of which can be on the YM / JE network.

Charges are nominal and negotiable as this is our first year of active operation.

If you choose to do a complete revamp of your website for all the [reasons mentioned here](https://amadeusweb.com/why-us/), charges would be:

* 20k for development and work until go-live.
* training, banner design and content loading extra at Rs 500 / 750 per hour.
* 3.5k for hosting from the 2nd year, platform fees waived.

</section><section>

## For Pillars

These are mentors and established program facilitators who may have a profile at either:

* joyfulearth.org/name/
* name.joyfulearth.org
* hubs.joyfulearth.org/location/name/

