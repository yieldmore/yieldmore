Welcome Students,

This is the first quarter of the Youth Empowerment Programme brought to you by the "Thought and Action Leadership Institute" a confluence of [AmadeusWeb](https://work.amadeusweb.com/) and [AwakenToLife](https://awakentolife.org/).

I am Imran, a software developer with over 20 years experience in a vast array of computer systems and applications.

I used to develop circuit boards in school and learnt Visual Basic 6 before pursuing my UG course in Electrical and Electronics Engineering which I graduated from in 2005.

On looking back, I can say I was always purpose oriented, driven by a hunger to explore and learn.

This is the essence of our program. There is no teaching / philosophy we can impart to make you have a healthy career, a great personal life or a strong civic sense. This has to come from within!

That's why we want our course to build a generation of "driven youth". You will be guided to achieve [these goals](%url%sophia/goals/).

Where do we begin? Start with

* A free-writing exercise.
* Begin work on your statement of purpose.
* Once you send these in, few of our staff will get back to you with a language improvement plan and the Life Visioning process.

The principal point of contact / convenor of our meetings will maintain a Google drive folder for all the students to have together and if you have access to a smart phone, you can create a folder for yourself and share with him / her.

If you are paper bound, he / she can take a photo of it and upload it to a folder he creates for you.

Remember "driven youth" means you should apply yourself diligently to the task of discovery, discipline and self improvement so it's up to you to make the most of our time.

Sessions will go with us all sitting in a circle or on video call and each one recapping their progress since the last session and framing a question which one of our facilitators will answer. Multiple rounds as time permits, and in the last round, you need to recap what the session gave you and what you plan to do until the next session. For all this, maintain notes either in paper or Google drive.

Always remember to make at least one free-writing essay and upload and share the link with us. And keep doing a self and progress analysis so you can course correct / put in more work as you decide.

Soon as you get the hang of sharing learnings and producing content, we will take up the creation of your website at learn.joyfulearth.org/group-name/student-name/

Don't worry if the blog is only in your mother tongue, we will share music, books and movies through which you can learn English better and come to appreciate world themes.

Imran Ali Namazi
+91.9841223313
1st Oct 2024

