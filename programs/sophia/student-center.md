<a name="goals"></a>
## Goals

In time we will guide you to these goals:

1. Develop the "Statement of Purpose"  "Life Visioning" or blueprint creation.

2. Work on core skills like written and oral communication, eventually feeling confident to have your own learner website, youtube channel and LinkedIn.

3. Factoring in on career options Plan A and Plan B besides understanding the landscape of your chosen area with a ringside view.

4. Devise a skill matrix based on your career options and chart out the learning resources and tasks needed to build competence in those areas. 

5. Learn intricacies of article writing, using compelling design and graphics for blogs, video creation - either with mobile or stills/voice-over . 

6. Give back to society. We call this ARYA - Awareness Resulting in Your Action.  

7. Explore basic programming,alternatives to Amadeus for web freelancing, a video making course, social entrepreneurship etc.

</section><section>

<a name="goals"></a>
## Notes

Please make a note of these:

* Remember how it felt to be a student and prepare to help train the next batch / document your journey online if you want to become a SME / influencer. Here, your diligence in capturing your learning process will be important.

* Each student will require to [serve an NGO](%url%arya/) for at least a month, so start planning who and how in your first week itself.

* An Evolving Goal Toolkit is listed out for you in [our brochure](https://docs.google.com/document/d/1FdCB29Wgom-z5EXjTQ5kCnykkv5Ig9zMCMJHi6Dp814/edit?usp=sharing). You can work on this both from a laptop as well as a phone (using your personal gmail account)

* After every session record a video recap and be ready to make a youtube and blog post out of it.

* Setup Amadeus at learn.yieldmore.org/group/your-social-handle/ remember this can be bilingual. If you are unsure about using this tool, you can use Google Sites.

* Don't worry about your current level of writing, speaking or presenting, this will improve over time.

* From your life vision and career choices, make a detailed list of skills.

* Always explore options and don't be reluctant to get into the details. Work isn't always what we want so allow for that.

* Technical skills are the easiest. Culture, mental strength, character and a strong civic sense take longer to build.

* Be aware of the challenges humanity is facing and try to find ways to alleviate that.

* Have an all rounder attitude to learning and steadily improve and expand your skills.

* We are all here to learn and grow together so don't feel shy.

* Guides are only there to paint the big picture, it's up to you to try, get into the details and make decisions.

* We are here to support you lifelong, so keep in touch and come back if you need anything.

</section><section>

<a name="updates"></a>
## Updates

1. A training assessment plan and an intro feedback mechanism will be brought in. This requires inputs from you
1. ATL will stress the need for students to earn and pay for the course. This will remove the "sense of entitlement". To this end, we are offering it to [Rural](%url%relief/) / Underprivileged Students for a base of Rs 500 per month.
1. Initial session conducted in October exposed multiple challenges, including teams preparedness. We are committed to work on this.

