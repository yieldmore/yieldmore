## Introduction

In this **tech-saturated**<br />
online world, the **challenge** is to:

* find one's voice.
* have it heard.
* find others to work with.
* build on a shared dream.

---

## Cascading Success

We wish to fill in "life and learning skill gaps"<br />
through a [unique proposal](https://docs.google.com/document/d/1GVvLlT8catkraPuR4yFM9_ShLbztu7RpFnsAhIMX8dA/edit?usp=sharing). We call it SOPHIA:

* S - Skilling
* O - Opportunity
* P - Pursuing
* H - Heartfully
* I - Innovative
* A - Ambitions

---

## About the Web Platform

Imran has been a:

* web enthusiast since 2000.
* has built sites [professionally](https://imran.yieldmore.org/career-past/), freelance and as a hobbyist.
* has crafted his own [web building framework](https://amadeusweb.com/).
* it can build an entire network of websites (ideal for batches of students / communities).

---

## Program details

A unique career / citizenry building program - [see brochure](https://docs.google.com/document/d/1FdCB29Wgom-z5EXjTQ5kCnykkv5Ig9zMCMJHi6Dp814/edit?usp=sharing), we will dwell upon these:

* skill building.
* life visioning.
* running of a healthy family.
* contribution to society.
* citizenry building.

---

## [Goals](%nodeLink%student-center/#goals) for students

* Life visioning and core skill development
* Explore career options and grow based on a skill matrix
* Showcase your learnings and your career - become a subject matter expert
* Think of giving back to society
* Learn to challenge/test everything, including what this program is telling you.

---

## How it works:

A Curtain Raiser session (online or to groups of more than 15). We will cover:

* what to expect from the course.
* students should be / willing to become self driven.
* attitudes and habits (who it is NOT for).
* an introduction session.
* walkthrough of "[possibilities](%nodeLink%possibilities/)".

---

## Session Conclusion

We will do:

* a free writing exercise (feedback only to those who enroll).
* an open-mic summary which can include a note on their career and life goals (for those interested).
* filling up of [enrollment form](#todo).

---

## Benefits to the students:

* Communication skills / online presence (when ready).
* Career planning and upskilling .
* Develop a healthy self-driven lifestyle for a lifetime of learning and growth.

---

## Benefits to the college/school/convenor:

* More empowered youth who would fulfil the college�s motto of technical excellence and service to society.

* Student created content remains on the college website network to showcase and increase visibility of student�s work, until they graduate and move it to their own domain.

* Outreach / CSR program possibilities as service to society, from the 3rd month on when the students begin work on [ARYA](%url%arya/).

---

## Financials (per month):

Rs 1500/- per student payable after attending the Curtain Raiser, at the intro enrolment session.
(Or) Rs 25,000 for a batch of 20 students sponsored by the organization.

Payment details: UPI: 9566166880@ybl

---

## Contact

Imran Ali Namazi
[work.amadeusweb.com](https://work.amadeusweb.com/)
[imran@amadeusweb.com](mailto:imran@amadeusweb.com?subject=sophia+enrollment+request)
+91-9841223313

