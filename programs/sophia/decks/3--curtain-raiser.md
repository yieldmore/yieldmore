(Planned as a session of 1 - 3 hours)

"Start with the END GOAL in mind"

So also our lives must be guided by an inner light that ultimately seeks a union with the divine..

# Phase 1

----

## Student 1 comes up and reads this - 5min

[joyfulearth.org/empowering-youth/](https://joyfulearth.org/empowering-youth/)

## Imran talks about core skills - 5 min

**Common skills to acquire**:

Presentation, Text / Video Blogging, Language Fluency & Speaking, Professional Network (LinkedIn) and Rapid Website Development.

## visit the enroll page - 10 mins

Join the whatsapp group

Fill details in the directory file. Everyone in single place.

Get a link to the enrollment and Starter kit

Get added to the je drive - 10 mins

# Students visit the website and read the brochure / it's shown on screen. - 5 min

## Student 2 reads out the goals. - 5min

<https://joyfulearth.org/empowering-youth/goals/>

## the goal toolkit from the brochure is discussed. - 5 min

## a few rounds of question and answer - 15 min

----

# Phase 2

## Students start filling the enrollment form / writing it on paper. - 20 mins

They need to show commitment to putting in the daily effort and be disciplined / self driven.

## Writing Exercise - 20 mins

Each student chooses one of these

* Free-writing - any topic
* Statement of Purpose
* A summary of <https://imran.yieldmore.org/keep-smiling/>

Poetry - individual or group� first time they prompt with a word and Imran writes, 2nd time we can do 2 lines each. If doing by themselves, can be either in English or Tamil.

## Students are asked to

- Send a free-writing full page at least 3 times a week. On paper which can later be typed up.
- Upload at least one video a week to their drive folder.
- Plan to have a linkedin profile where they document their journey and start aligning themselves to their chosen career path.
- Learn the skills of communication, presentation and advocacy.
- Think of learning and live project tasks each week.
- Plan to help a Charity from the 2nd month on.
- Market the relevance of the center and the program with a thought to become facilitators themselves.
- Watch <https://youtu.be/wX78iKhInsc>� think deep and tell how you want your "education" to have been improved.
- Write an essay why they would like to be a part of a [https://GoodGeneration.org](https://goodgeneration.org).
- Start planning about their first public speech.

# Future plans - 15 mins

* Discussion on do we charge, what is workable and supports trainer (imran) and a local co-facilitator.
* Can we become a web and media team so we add local businesses to the hub? Will NGOs pay us? What can we offer them both?
* Convenor is asked to find a co facilitator who will be present every weekend for 2 to 4 hours a day.

