# Demo 1 - 40 mins

Once enough show a keenness to have a personal online space

Create a location\@hubs.joyfulearth.org and sign up for a free Google account with it.

A bitbucket account is made for use with this email.

Amadeus is introduced. Main page, docs page and website setup.

Each student picks a handle (by editing the directory page) and a Google folder is shared with them so they can author / upload stuff

A site for the center is built on the spot, this takes about 20 mins. For eg, localhost/hubs/location/about/

Students are asked one by 1 to recap the session each on a video that they upload to Google drive. This can be done later and shared on the group.

The convenor recaps the session in a video that we embed from Google drive into the location's pulse site's blog. So hubs.joyfulearth.org/location/pulse/october-2024/

Changes are reviewed and committed.

**20 mins into Hour 3**

# Demo 2 - 15 mins

designing a poster for the program and uploading to site.

# In conclusion - 10 mins

Organizer exhorts the students to put in the efforts so they can benefit the most.

Imran says he's always there to help. And will reply to queries on email and the whatsapp group.

