(from Jan 2023 and earlier)

# What is this Course

Based on my enthusiasm, love for kids and the bright future they promise us and experience with story, I have been asked to take a small 3 month program in "Creative Expression, Perspectives and Mentoring"

This section, dubbed "LAST" or "Love All Stories Timelessly" are my notes, learnings and class content which should form the basis of similar future programs.


# Why Enter into Story

Stories give a distinct flavour and colour to our lives, always reminding us of a certain goal, viewpoint or fight. They offer us perspective, exhilarating heights and zapping lows. They can, with time, and when entered in the right spirit, prime our emotional and spiritual selves, filling us with purpose and enthusiasm.

They often call out to us to treat our fellow beings right, an attempt of various authors to right the wrongs of history. For after all, as is said - the pen is mightier than the sword.

Ours may be a wonderful world, but there is also dissonance in the Great Music, making this, what some may call a flawed creation, but then, along comes this spiritual giant - **Sri Aurobindo** - and says "perfection has to be worked out".

What that means is, we do not yet know how the story will end� But if we consecrate our daily action with a higher aspiration and blend our daily thoughts with the visions of countless men and women of near divine stature, we can build a world of abundance.

All it needs is an accepted invitation into the story of humanity, and oh, what a painful past she's had and what pains she goes through everyday.![](https://lh7-rt.googleusercontent.com/docsz/AD_4nXePEf7nxd6KQ-SIEt3BHNU_cq-MREYx1zUXf6BSygbjES8tUKlhFJy3V1pasiGuWFm8xj4TE17tXV8kkyeEK9shW_RESYK1sNTCzUjPZccnAKa11rebZ8WkNeR1AEUHLsOjfitaJ_IsVz953VU5Bpw7nT8?key=CIxe5PTHrBWgV8JOj66vfw)

Later in that same book on Yoga, Sri Aurobindo talks about "fulfilling existence in God" - this is the purpose of our work here in this lifetime, to equip ourselves with the right values and skills that we may in some small way understand and together, improve the human condition.


# The Wish Class - [c. 2020](https://2020.yieldmore.org/wish-class/)

Dear Parent,

We want you to feel safe leaving your children in our care. We want to know what hopes and dreams you have for them and we want to hear from them directly what all they would like to do.

We want to pique their curiosity for the world around us, make them sensitive to things that are happening, help them introspect, feel free talking about fears and concerns, life's plans and doubts.

We have the following objectives

1\) Make them feel comfortable and supported

2\) Help them choose learning and development activities

3\) Guide their thinking process

4\) Expose them to the arts and culture. This includes books and music.

5\) Help them shed their inhibitions in expressing themselves

6\) Show them how they can make their lives purposeful and meaningful

7\) Explore their creativity including articles, essays, poems, painting, videos etc.

8\) Put them on a compassionate, kindly path

9\) Prepare them for adulthood and expose them to world views.

10\) Help then organize their various wishes and make a plan to achieve them.

At every stage of this, we encourage parent participation and want you to feel comfortable with us mentoring them.

We call this a "Wish Class".

Warmly,

Imran, Dec 3, 2020


## Wish Class - Format

Class could contain the following elements

- Invocation

- choice of prayer

- homework review

- 5 questions

- songs discovered

- journal discussion

- one goal and how to plan

- writing time

- topic of the day

- review of hour gone


# Creative Expression - [c.2020](https://2020.yieldmore.org/creative-expression/)

## Introduction

We want the best for our children. We want them to grow up happy, capable and purposeful, doing things they can feel proud about.

We also want them to develop emotionally and be able to express their thoughts and individuality.

We need to work together and give them a good base. We need to give parents and teachers the power to shape their children's lives.


## Support for Creative Expression

Introducing the PACT ([COMMUNITY: parent teacher networking](https://community.yieldmore.org/group/education-forum-of-ym)) initiative of YieldMore.org and how through their participation, support,and encouragement, our 8 week Creative Expression Program can help trigger a more free-flowing, inspired phase in the children's lives.

We believe in enabling parent / teacher communities to jointly consider the holistic development of their children, giving them the right exposure, activities and encouragement necessary to blossom into joyful, talented and purposeful individuals.

An ongoing task for schools, PTAs and parent circles, our first priority is to orient and network these. As we discover programs and activities, we share these and the groups, individual parents / teachers make a decision for the budding youngsters. The flow of suggestions is two way, with parents often contributing ideas to our network at large.


## Other Supporting Programs

In a separate article, we will deal with brain science and study, but for now, suffice it to say that there are [programs like Spirits Journey and Brainsync](https://2020.yieldmore.org/audio-nootropics) that help increase the level of function of both the left brain (Logic) and right brain (Creativity) in harmony and BOTH sides developed. Thus, for the children, Artful Skill and Skillful Art is developed.


## About the Process

We proffer a 4 step process for developing a child's Creative Expression - one of Explore\*1, Experience, Emote, Express.

Character is important and that comes from exposure (exploring) to the right ideas and stories, giving them the context (experience) to develop their own views (emotional proclivities) , skills and ambitions.

Music and poems soothe and inspire. Worldview development from exposure to Stories and Movies. Through curation, we give children an opportunity to explore this wonderful world of songs/movies/books. In the last stage, they are encouraged to express themselves.

Ideally an ongoing process right through school and college life, we have condensed this into an 8 week storytelling and poetry writing program to initiate this viz: [2020 YM/storytelling-and-poetry/](https://2020.yieldmore.org/storytelling-and-poetry/)

