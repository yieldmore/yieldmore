## Why this Course?

Do you feel:

* your job prospects are not that good.
* your communication and confidence needs improvement.
* you are unsure what to study further and what career options to pick.
* you need to develop common and industry specific skills.
* you worry about your personal and family life unfolding.
* you wonder what will be your contribution to society, unsure what to do.

Then this program is for you :)

<!--more-->

</section><section>

## Motto: Lovingly Guiding Our Youth!

**Vision** - A generation of youth not only with the right skills but also the sensitivity to support whatever cause that is close to their heart.

**Mission** - Share the knowledge from senior professionals to empower the youth to make a career for themselves in any field of their choosing.

</section><section>

## Enroll 

This form, available as a Google Document - [Student Enrollment and Starter Kit](https://docs.google.com/document/d/1Mw4qTbuw-FR51cVLmiZcJ58pKgBpceh85OljqZGEiZo/edit?usp=drive_link) can be copied and shared back with us at [imran@amadeusweb.com](https://imran@amadeusweb.com/) is listed below.

It covers:

* Basic Details
* Attitide and Commitment
* Objectives
* Self Assessment
* Free Writing
* Purpose and Life Goals
* Career Options and Skills to Acquire
* Further Items to Discuss

Once you fill it, you can join the [WhatsApp Chat here](https://chat.whatsapp.com/LBSS8Ar5sIK4u4GkliDOfz).

If you are interested in having your own learning site at network.yieldmore.org, contact us.

----

Join our LinkedIn group where we help you build your online presence from Day 1.
[https://www.linkedin.com/groups/14012871](https://www.linkedin.com/groups/14012871)

Contact Imran at [team@yieldmore.org](mailto:team@yieldmore.org) or 91.9841223313

</section><section>

## Updates

1. Offers will be made to schools for the underprivileged once the details are sorted out.
1. November saw the creation of this new "Alliance" network, and interest in developing this program was resparked.
1. October also had us make a proposal to a college (they are busy with exams till Jan).
1. Then in October we tried it in a Rural setting but found the basic skills were very poor.
1. This was first [a LinkedIn post](https://www.linkedin.com/feed/update/urn:li:activity:7229510521262104577/) in August 2024.

