## Jeevan Vidya

Jeevan Vidya, or [co-existence philosophy](https://jvidya.com/) is finding applications in multiple areas.

Earlier, we had taken permission to upload several pdfs on the [YieldMore Archives](https://archives.yieldmore.org/jeevan-vidya-english/).<!--TODO: UX of Archives Site to be improved-->

Now, we are hoping to restart a dialogue with them, offering to showcase their work still further to meet a global audience - in any which way we can!
