<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

define('SITEPATH', __DIR__);

include_once '../../amadeus/framework/entry.php';

am_vars([
	'flavour' => 'yieldmore',
	'skip-footer' => true, //else both ym + light place a footer
	'engine' => 'light',

	'theme' => 'artist',
	'auto-render' => true,

	'site-home-in-menu' => true,
	'use-menu-files' => true,

	'autofix-encoding' => true,

	'no-engage' => true,
	'no-assistant' => true,

	'description' => 'A network of those who care and act!',
	'og:image' => '%url%assets/yieldmore-opengraph.jpg',

	'social' => [
		//[ 'type' => 'group', 'link' => 'https://groups.io/g/yieldmore/topics', 'name' => 'Mailing List from groups.io' ],
		//[ 'type' => 'facebook', 'link' => 'https://www.facebook.com/groups/yieldmore/', 'name' => 'facebook: main group' ],
		[ 'type' => 'youtube', 'link' => 'https://www.youtube.com/@YieldMore', 'name' => 'youtube: main channel love / new' ],
		[ 'type' => 'youtube', 'link' => 'https://www.youtube.com/@FacesOfYieldMore', 'name' => 'youtube: faces / 2018 and 2019' ],
		[ 'type' => 'youtube', 'link' => 'https://www.youtube.com/@ImranYieldsMore', 'name' => 'youtube: legacy / imran' ],
		[ 'type' => 'linkedin','link' => 'https://www.linkedin.com/in/imran-ali-namazi/', 'name' => 'Imran on LinkedIn' ],
		[ 'type' => 'yieldmore','link' => 'https://ideas2.yieldmore.org/', 'name' => 'Ideas volume 2 (2023) by YieldMore.org' ],
		[ 'type' => 'yieldmore','link' => 'https://ideas.yieldmore.org/', 'name' => 'Ideas (2022) by YieldMore.org' ],
		[ 'type' => 'yieldmore','link' => 'https://archives.yieldmore.org/sitemap/', 'name' => 'Archives (2021) by YieldMore.org' ],
		[ 'type' => 'yieldmore','link' => 'https://legacy.yieldmore.org/sitemap/', 'name' => 'Legacy (2013) by YieldMore.org' ],
		[ 'type' => 'yieldmore','link' => 'https://imran.yieldmore.org/tags/all/', 'name' => 'Imran\'s Prose and Poetry @ YieldMore.org' ],
		//[ 'type' => 'linkedin','link' => 'https://www.linkedin.com/company/yieldmore-and-joyfulearth/', 'name' => 'YieldMore.org and JoyfulEarth.org Page on LinkedIn' ],
	],
]);

runFwk('site');
