Think good. Express love
A website without clutter.
A larger place to dovetail into
Promote ideas and activities
Help others and heal hurts
Spawn some of the 1000 loving movements
Share inspiring and useful information
Find people to help and mentor you
Find good habits and healthy perspectives

----
Don't compete, coexist
Promote the works of healers, teachers, social workers
Share your thoughts
Devote quality time to friends and yourself
Recreational writing / connect with people
Pay it forward and grow happy
Help across all strata of society
Be a harbinger of change and a force for good
Be at peace with yourself
