# Climate and Sustainability Consultant

<b>Passion</b>: To develop local solutions in adapting the effects of climate change and build resilience which majorly
involves the core survival areas such as water, food, shelter and energy.
