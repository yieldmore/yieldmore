# Climate and Sustainability Consultant

<b>Passion</b>: To develop local solutions in adapting the effects of climate change and build resilience which majorly
involves the core survival areas such as water, food, shelter and energy.

</section><section>

# Past Activities

2. Involved in Community based environment development (CBED) program with Chennai metropolitan development authority (CMDA) in which community participation is the highlight. Associated with Pond renovation, Park development, Burial ground development and Storm water drain project works in the suburbs of Chennai.
3. Market support to Farmer Producer Companies in a few districts of Tamil Nadu. Guidance for direct selling to consumers; to keep a customer base for regular business.
5. Urban market development for Safe and nutritious food. Also guided many new entrepreneurs to build their organic retail business. Built safe food awareness among corporate employees, students & residential communities.

For more, [see my biodata](https://drive.google.com/file/d/17SeHTJMHTWNK8yz74siyfYDhLHp2UOCi/view?usp=drive_link) or [my linkedin profile](https://www.linkedin.com/in/radhakrishnan-subbuduraisamy/).

</section><section>

# Current Activities and Interests

* Building a climate resilient model farm at my native place in Dindigal. More details to follow.
* Available to consult on farm end to end consulting from location selection to design to building and operating and even marketing the produce.
* Seeking to imrove the areas of [___](#hashtag) with [my fellowship](#linkedin-announcement). Ask for [reports here](https://docs.google.com/spreadsheets/d/1hRNLXMmCn3WAtI0TVZtv7Mxe7bhXDHo7wGV4iEvxFTs/edit?usp=drive_link).

</section><section>

# Abilities & Skill sets.

* To design creative solutions for Climate action in line with Adaptation and Resilience.
* Planning and implementation of Integrated Sustainable development Projects.
* To make rural and urban communities participate in conservation of nature with right understanding.
* Inclusion of Sustainability in existing organisation's process for efficient results, linking SDGs.
* Liaison with Nabard, TERI, UN Habitat, UNICEF, ENVIS, Dept. of Environment & Forests and MNCs.
* To organize Community events, Rallies & Campaigns to create awareness.
* To engage and communicate with diverse population and groups of all sizes.
* To introduce the rising opportunities in the sustainable development field to students and entrepreneurs.

