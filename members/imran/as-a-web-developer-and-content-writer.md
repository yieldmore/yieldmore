<!--engage: SITE //engage-->

## Imran's Introduction

* As a seasoned web developer and [solution architect](https://imran.yieldmore.org/career-past/), I can be retained for website, webapp and mobile development / bootstrapping a large project by writing infrastructure code.
* My preference is to conscious businesses and individuals.
* I also recently started [showing youngsters](%url%working-for-you/empowering-and-mentoring-youth/) how to go online.
* I believe not just in tech, aesthetic and content but a [360 view of what Charities are doing](%url%working-for-you/towards-your-cause/) and how to help them.
* I no longer use wordpress, instead preferring my own [AmadeusWeb](https://amadeusweb.com/).

## Using AmadeusWeb for your Website

For web development, I strongly suggest we use my [amadeusweb.com](https://amadeusweb.com/) for long term content creation and trackability. Apart from [Why Us](https://amadeusweb.com/reasons/), I can explain with example how other sites on the AmadeusWeb/YieldMore network are built / maintained elegantly and easily.

It lets you organize in easily trackable folders, all the copy for your website. It also lets you maintain updates in a simple sheet which can include links to where those updates have happened on social media.

* Initially a menu structure is discussed and finalized (top level menu / mini site).
* The site is setup, folders with placeholder files are created so the menu fills up.
* Details are pushed into the appropriate files.
* A discussion is had on what to enhance, what pictures are needed and whats to be posted socially etc.

## My Web Building Summary

* I have built [enterprise websites](https://www.linkedin.com/in/ianamazi/) in asp.net - both in 2006 (englishtown) and 2009 (sdworx).
* From 2008 to 2013, at work it was mostly windows applications - framework and core development - which trained me to think of code organization and single time code definition.
* I've always had a burgeoning hobbyist / freelance web development - right from my first php website in 2011 to [wordpress](https://bitbucket.org/ianamazi/wp-cselian) and [YiiFramework freelancing](https://bitbucket.org/cselian/e-vend).
* 2011 saw me develop my first [web builder framework](https://github.com/yieldmore/MicroViC/) (also file based) with which I built 6 or 7 fun sites - most notably TLR.
* I have published the code of a [series of 40+ websites](https://code.amadeusweb.com/) and hope that students will learn to build features from them.
* In 2019 Oct, I started [developing Amadeus - see stats at bottom](https://ideas.yieldmore.org/?debug=1) as a simple file based microframework and web builder.

## Ways to engage

* Join the youth empowerment program / refer it to your school, college or friends.
* Ask for a 1-1 consulting so we can discuss synergies.
* Typically I charge 25k+ for a full fledged website and another 5k yearly hosting.
* Am ok to discount my charges if we can find mutual interests and you promote me on your website, social media  and by word of mouth.

## Social Media and Promotion

1. A youtube channel is made or @joyfulearth is used. Here be careful when a vendor / news channel is featuring you as ideally it should be on your channel.
2. Videos are embedded where needed.
3. Canva and Abode Express are used for images and videos.
4. Social media is given importance, this includes LinkedIn too.

