I'm a passionate, smart and eloquent developer who understands well the principles that go into good architecture. I want now to mentor teams on habits, process, documentation and the break down of tasks - to make the outcomes better and less painful.

After [20 years in IT](https://imran.yieldmore.org/career-past/) and a hands-on builder of rich features and architectural code for consumption by other developers.

## Web and Content

As builder of my own framework AmadeusWeb.com - it can be used to build file driven websites saving people from the hassle of deploying content changes, security concerns etc. See more [about consulting](%url%imran/as-a-web-developer-and-content-writer/) / what [the builder](https://amadeusweb.com/builder/) is about.

<!--more-->

</section><section>

## IT Solution Architect

Today I can be engaged [as an IT consultant](%url%imran/as-an-it-consultant/) for

* Core Feature / Framework Development
* Team Mentoring and Process Refinements
* Quality Initiatives and Code Reviews
* Web / Mobile App Programming

</section><section>

## Lateral Expansion

As [a poet](https://imran.yieldmore.org/poems/) with a sense of [humanity's destiny](https://imran.yieldmore.org/all/for/msa/), I tell stories that **offer hope** and perspective.

An aspiring social entrepreneur, here are [my ideas](https://ideas.yieldmore.org/) and [newer ideas](https://ideas2.yieldmore.org/).

I am exploring other dimensions of my value add so:

* [Youth Trainer](%url%sophia/)
* [Youth on Cause Advocacy / Promotion](%url%arya/)
* [IT Consultant to Social Work Orgs](%url%imran/as-a-consultant-for-charities/) for not just web and writing, but also tools and process.

----

## Content Writer / Ideator

* I have written more than [500 poems](https://imran.yieldmore.org/poems/) and [75 essays](https://imran.yieldmore.org/prose/) and would like to work for you in a creative / ideative way.
* See my nearly [20 year history](https://ideas2.yieldmore.org/august-2024/30th-about-imran/) in dreaming which led me to become a writer and built my own tech.

