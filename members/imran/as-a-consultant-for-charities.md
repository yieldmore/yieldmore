<!--engage: SITE //engage-->

Dear Charity / NGO Founder,

I, Imran have lent my web skills to Charities in the past and built a [number of websites](https://code.amadeusweb.com/) for them / friends. I also have been working as a [programmer since 2006](https://imran.yieldmore.org/career-past/).

Also see what I can do as a [as a web developer and content writer](../as-a-web-developer-and-content-writer/).

Today I feel I can utilize the experience in IT as tech manager to do more than just build websites but help in all areas of functioning.

%engage-note-above%

## At a High Level:

* Project Management and Volunteer Selection.
* Process Development and Training.
* Personal Assistance Services to Executives.
* Content Writing and Graphics Design, Web and Technology.
* Videos with stills and voice overs.
* Creation of Internal Documents / Training and Knowledge Bases.
* Use of a filing system like Google Drive.
* Collaboration / Brainstorming and being a Sounding Board.
* Guiding a Marketing Team (a skill we dont as yet possess).

## Project Review and New Way of Working

1. Take stock of all projects, end goals, short and middle term.
2. Start thinking of Milestone and Sprint (Bi-weekly) goals and tasks. Impediments and Retrospectives are clearly tracked.
3. capacity and velocity is also planned. Story points are estimated on the first day of the sprint (1st and 16th of each month)
4. Explore and let core team decide on project/task management and reporting tools.
5. A planning and delegation process for task communication using [Jira](https://www.youtube.com/watch?v=rFIyK8aDsGc), [Wrike](https://www.wrike.com/project-management-guide/middle-planning-the-project/), Asana, Bug Genie or just **Google Drive / Docs**.
6. List out all team members and skills, and projects assigned.
7. Make an umbrella consolidation of brand/team when multiple projects have grown apart.

## New Process Rollout

1. Introduce all team members to each other. Onboard them to new way of working / documenting and communicating - allow a predetermined timeframe in which all should be up and running.
2. Push for daily stand-ups / beginning and end of day reports (written not oral).
3. Start a WhatsApp community for internal use and outreach or decide to use GMail Spaces.
4. Team complementing - offer unpaid internships to social work and visual communication students (for example) by exchanging personal growth and mentoring for their time.
5. Streamline communication and have an internal documentation project / physical and virtual asset inventory.

%engage-note%

