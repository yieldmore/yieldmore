<!--engage: SITE //engage-->

## Consulting Availability

A [Solution Architect](https://imran.yieldmore.org/career-past/), I am looking for:

* Part time development roles.
* Mentoring developers in your team.
* Engaging in architecture dicusssions and sharing my skills / experience with seniors and leads.
* Process overhaul and quality initiatives.

A detailed form of what all I have to offer, ideas and initiatives can be filled at: [ideas2.yieldmore.org/at-work](https://ideas2.yieldmore.org/at-work/).

----

## Professional Summary:

* Now completing 24 years [as a programmer](https://code.amadeusweb.com/).
* Have written clean, artisan code all my career.
* Write framework / infrastructure code for use by other developers.
* Can be retained to help with core development / [quality](https://ideas2.yieldmore.org/at-work/brainstorming/) and [process](https://ideas.yieldmore.org/work/organizational-excellence/).
* Interested in putting my diverse skills for quality improvements in any IT Company.

----

## Ideas for the Workplace

* [Since 2013](https://ideas.yieldmore.org/work/)
* [Since 2022](https://ideas2.yieldmore.org/at-work/)

%engage-note%

