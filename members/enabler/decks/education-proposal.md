# Educating<br />a generation<br />that thrives!

<hr /><br />

## A CSR Funding Proposal

---

## [Youth Empowerment](https://joyfulearth.org/go/youth/)
<hr />

- Basic Skills
- Life Visioning
- Career Exploration
- Skill Deep Dive
- Agency and Self Advocacy
- Internship with a Charity

<hr />
Facilitator: [Imran](https://imran.yieldmore.org/working-for-you/)

---

## [Be a Giver (of Jobs))](https://joyfulearth.org/go/biz/)
<hr />

- Visionary
- Spirit of Adventure
- Open to New Ideas
- Lead by Example
- Inspires and Motivates
- Communication and People Skill
 
<hr />
Facilitator: [Ghulam Mustafa Badsha](https://joyfulearth.org/enabler/about-mustafa/)
 
---
 
## Project Coordination
<hr />

- Oversees Course Material Creation
- Selects a Marketing Partner
- Uses extensive existing network
- Handles Administrative Duties
- Has additional training material, chargeable separately
- Reporting and Finances
 
 <hr />
Handled by: Team at [AwakenToLife.org](https://awakentolife.org)

---

## Cost Breakdown
<hr />

- 6mo Salary to Mustafa - Rs 15l
- 6mo Salary to Imran - Rs 15l
- Content / Media Creation - Rs 4l
- Coordination Salary - Rs 2l
- Marketing Budget - Rs 2l
- Admin Expenses - 2l

---

## Cost and Terms:
<hr />

- Rs 40 lakhs for a 6mo program
- Half payable before we begin
- Balance after 3mo

- 1 month time to prepare the course material and plan for marketing

---

## Contact
<hr />

* Mustafa - [+91-98400-23721](tel:+91-98400-23721)
* Imran - [team@yieldmore.org](mailto:team@yieldmore.org)

