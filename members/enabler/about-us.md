<div class="row mb-20 contact">
	<div class="col-md-12"><h2>Mustafa Badsha</h2></div>
	<div class="col-md-4">
		<a class="autosize icon-whatsapp" href="//wa.me/+919840023721">+91 98400 23721</a>
	</div>
	<div class="col-md-4">
		<a class="autosize icon-phone" href="tel:+919840023721">+91 98400 23721</a>
	</div>
	<div class="col-md-4">
		<a class="autosize icon-email" href="mailto:badshasalamath@gmail.com" target="_blank">badshasalamath@gmail.com</a>
	</div>
</div>

<b class="name">ENABLER</b> is an <b>emerging movement</b> and a <b>voluntary welfare initiative</b> committed to <b>empower marginalised groups</b> and <b>members of economically weaker sections</b> of society with knowledge, skills, and a host of resources and referrals needed to strive and excel in the ever-changing world of today.

Initiated, propelled and powered by professionals with vision, energy, experience, resources and the spirit to rise above the usual, <b class="name">ENABLER</b> is rapidly gathering momentum as a socio-economic movement, creating opportunities to move forward.

The aim of <b class="name">ENABLER</b> is to produce <b>high caliber individuals</b> committed to establish a society based on <b>Truth, Justice, Equality, and Fairness</b>, beyond the borders of social class, caste, creed and religion viewing humanity and the World as one Global Entity.

<!--more-->

<hr />

<a class="g-doc" href="https://docs.google.com/document/d/1EGZWGOlUBgZqeRUTmLpSYOV7aslrD25EUfwbiWAASrk/edit?usp=sharing" target="_blank">Open the Profile of Mustafa Badsha</a>

## Our Services
* "Economic stimulus" programs and activities</li>
* Crisis intervention</li>
* Individual / Family counselling</li>
* Sponsorship of students / trainees pursuing careers and jobs</li>
* Design and development of CSR (Corporate Social Responsibility) projects</li>
* Referral services (related to internship, training and employment)</li>
* Information & assistance</li>
* Resource mobilisation (both material and financial)</li>
* Production of documentary films related to socio-economic issues</li>
* Production of teaching materials for print and digital purposes</li>

## More Services
* Liaison / representation with government departments / hospitals / police / NGOs / Employer etc.</li>
* News producers for Social Media</li>
* Working with entrepreneurs to produce realistic curriculum to make education and training more relevant, purposeful and meaningful</li>
* Research - Educational research / trade / consumer research</li>
* Supporting garbage segregation and recycling initiatives</li>
* Repositioning business and brands</li>
* Grooming aspiring individuals to become responsible and accountable leaders</li>

